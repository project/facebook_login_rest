<?php

namespace Drupal\facebook_login_rest\Plugin\rest\resource;

use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\user\Entity\User;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource for login with Facebook Token.
 *
 * @RestResource(
 *   id = "facebook_login_rest",
 *   label = @Translation("Facebook Login REST"),
 *   uri_paths = {
 *     "canonical" = "/user/facebook/login",
 *     "https://www.drupal.org/link-relations/create" = "/user/facebook/login"
 *   }
 * )
 */
class FacebookLoginRestResource extends ResourceBase {

  /**
   * The CSRF token generator.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfToken;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * Constructs a new Facebook Login Rest Resource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrf_token
   *   The CSRF token generator.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    CsrfTokenGenerator $csrf_token,
    RouteProviderInterface $route_provider
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->csrfToken = $csrf_token;
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('csrf_token'),
      $container->get('router.route_provider')
    );
  }

  /**
   * Responds to POST requests.
   */
  public function post(array $data) {

    if (!empty($data['access_token'])) {

      $config = \Drupal::config('facebook_login_rest.settings.facebook_login');

      if ($config->get('app_id') && $config->get('app_secret')) {

        $facebook = new Facebook([
          'app_id' => $config->get('app_id'),
          'app_secret' => $config->get('app_secret'),
          'default_graph_version' => 'v3.1',
          'default_access_token' => $data['access_token'],
        ]);

        try {
          // Get the \Facebook\GraphNodes\GraphUser object for the current user.
          $response = $facebook->get('/me?fields=id,email');
          $me = $response->getGraphUser();

          // Check if user exists with email.
          $account = user_load_by_mail($me['email']);
          if (!empty($me['email']) && !empty($account)) {
            // Load existing user.
            $user = User::load($account->get('uid')->value);
          }
          else {
            // Create user.
            $user = User::create();
            $user->setPassword(user_password(10));
            $user->enforceIsNew();
            $user->setEmail($me['email']);
            $user->setUsername($me['email']);

            if (!empty($me['id'])) {
              $user->set('field_facebook_id', $me['id']);
            }

            $user->activate();
            $user->save();
          }

          // Auth user.
          user_login_finalize($user);

          // Send basic metadata about the logged in user.
          // Like \Drupal\user\Controller\UserAuthenticationController::login.
          $response_data = [];
          if ($user->get('uid')->access('view', $user)) {
            $response_data['current_user']['uid'] = $user->id();
          }
          if ($user->get('roles')->access('view', $user)) {
            $response_data['current_user']['roles'] = $user->getRoles();
          }
          if ($user->get('name')->access('view', $user)) {
            $response_data['current_user']['name'] = $user->getAccountName();
          }
          $response_data['csrf_token'] = $this->csrfToken->get('rest');

          $logout_route = $this->routeProvider->getRouteByName('user.logout.http');
          // Trim '/' off path to match \Drupal\Core\Access\CsrfAccessCheck.
          $logout_path = ltrim($logout_route->getPath(), '/');
          $response_data['logout_token'] = $this->csrfToken->get($logout_path);

          // Add User uuid.
          $response_data['current_user']['uuid'] = $user->uuid();

          // Make the response use the user's cacheability metadata.
          $response = new ResourceResponse($response_data);
          $response->addCacheableDependency($user);

          return $response;

        }
        catch (FacebookSDKException $e) {

          // When validation fails or other local issues.
          return new ModifiedResourceResponse(
            [
              'code' => $e->getCode(),
              'message' => $this->t('Facebook SDK returned an error: @message', [
                '@message' => $e->getMessage(),
              ]),
            ], 401);
        }
        catch (FacebookResponseException $e) {

          // When Graph returns an error.
          return new ModifiedResourceResponse(
            [
              'code' => $e->getCode(),
              'message' => $this->t('Graph returned an error: @message', [
                '@message' => $e->getMessage(),
              ]),
            ], 401);
        }
      }
      else {
        return new ModifiedResourceResponse($this->t('Facebook App Id or App Secret is missing.'), 401);
      }
    }
    else {
      return new ModifiedResourceResponse($this->t('Token is missing.'), 401);
    }
  }

}
