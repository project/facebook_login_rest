# FACEBOOK LOGIN REST

Facebook Login REST provides a rest resource for login with Facebook Token.

## INTRODUCTION

* For a full description of the module, visit the project page: 
  https://drupal.org/project/facebook_login_rest
* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/facebook_login_rest
  
## REQUIREMENTS

* [Facebook Graph SDK](https://github.com/facebookarchive/php-graph-sdk)

 
## RECOMMENDED MODULES

Markdown filter (https://www.drupal.org/project/markdown):
When enabled, display of the project's README.md help will be rendered
with markdown.

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.

## CONFIGURATION

The module has an admin menu in /admin/config/people/facebook-login-rest and there are modifiable settings to add app_id and app_secret.  
There is no configuration.  
When enabled, the module will prevent the links from appearing. 
To get the links back, disable the module and clear caches.


## MAINTAINERS

This project has been sponsored by:

[Insign](https://www.drupal.org/insign)
